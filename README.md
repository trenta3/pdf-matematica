# Pdf delle dispense che ho studiato di matematica
Questo repository raccoglie per esame le dispense che ho studiato, in modo che se in futuro avrò qualche dubbio potrò andarle a rivedere con facilità.

## Corrispondenza tra sigle ed esami
* TNE: Teoria dei Numeri Elementare
* SM: Statistica Matematica
* GAAL: Geometria Analitica ed Algebra Lineare
* TAN1: Teoria Algebrica dei Numeri I
* TCG: Teoria dei Campi e di Galois
* AM3: Analisi Matematica 3
* ANALNUM: Analisi Numerica con Laboratorio
* EPS: Elementi di Probabilità e Statistica
* ETI: Elementi di Teoria degli insiemi
* TM: Teoria dei Modelli
* EGA: Elementi di Geometria Algebrica
* ISTALG: Istituzioni di Algebra
* ARIT: Aritmetica
* ETA: Elementi di Topologia Algebrica
* ALG1: Algebra 1
* ISTGEO: Istituzioni di Geometria
* TCC: Teoria dei Codici e Crittografia
* PROB: Probabilità
* COMPLMAT: Complementi di Matematica (Interno)
* QINFO1: Quantum Information I (interno)
* TMO: Teoria e Metodi dell'Ottimizzazione
